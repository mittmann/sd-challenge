export FLASK_APP = server/app.py

install:
	@pip install -r requirements/requirements.txt

test_install: install
	@pip install -r requirements/requirements_test.txt

test: test_install
	@py.test

run:
	@flask run --host=0.0.0.0

"""
    Flask Blueprint module for API management
"""
from flask import Blueprint
from flask import request, jsonify, Response
from server.customer.views import CustomerView


api = Blueprint('api', __name__)


# Routes
@api.route('/', methods=['GET'])
def index():
    return 'API'


@api.route('/customers/', methods=['GET', 'POST'])
def customers():
    """
        Customers endpoint
        Methods:
        - GET:  Return a JSON with all customers registered in db
        - POST: Insert a new customer in db, and return status 201 - Created,
                case successful or status 400 - Bad Request, in case request
                data not satisfact the requisites
    """
    if request.method == 'GET':
        customer_list = CustomerView.get_customer()
        return jsonify(customer_list)

    else:
        CustomerView.insert_customer()
        return Response('', status=201)


@api.route('/customers/<int:_id>', methods=['GET', 'PUT', 'DELETE'])
def customer(_id):
    """
        Customer endpoint - Should contain a customer ID
        Methods:
        - GET:  Return a JSON with the customer
        - PUT:  Update the customer in db, and return status 200
                case successful or status 400 - Bad Request, in case request
                data not satisfact the requisites
        - DELETE: Delete the customer from db
    """
    if request.method == 'GET':
        _customer = CustomerView.get_customer(_id)
        return jsonify(_customer)

    elif request.method == 'PUT':
        CustomerView.update_customer(_id)

    elif request.method == 'DELETE':
        CustomerView.delete_customer(_id)

    return Response(status=200)

"""
    Flask APP Server
    App responsible for return of web page, API and database management.
"""
from flask import Flask, render_template, send_from_directory
from flask.ext.sqlalchemy import SQLAlchemy

# Create Flask APP
app = Flask('sb-challenge', template_folder='www')
app.config.from_object('server.settings')
print(app.config)

# Instanciate DB
db = SQLAlchemy(app)

# Register Blueprints
from server.api_server import api
app.register_blueprint(api, url_prefix='/api/v1')


def create_db():
    db.create_all()

create_db()

# Webpage Routes
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/static/<path>', methods=['GET'])
def static_files(path):
    return send_from_directory(app.config['STATIC_DIR'], path)


if __name__ == '__main__':
    app.run(debug=True)

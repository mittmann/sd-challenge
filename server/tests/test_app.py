
import unittest
import json
from server.app import app, create_db


class TestApp(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        # Set SQLite3 to memory
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'

        with app.app_context():
            create_db()
        self.client = app.test_client()


    def test_index(self):
        """
            Test index page, only status code == 200
        """
        response = self.client.get('/')
        self.assertEqual(200, response.status_code)


    def test_api_methods(self):
        """
            Test methods alloweds and not alloweds on endpoints
        """
        get = self.client.get('/api/v1/')
        put = self.client.put('/api/v1/')
        post = self.client.post('/api/v1/')
        delete = self.client.delete('/api/v1/')
        self.assertEqual(200, get.status_code)
        self.assertEqual(405, put.status_code)
        self.assertEqual(405, post.status_code)
        self.assertEqual(405, delete.status_code)

        get = self.client.get('/api/v1/customers/')
        put = self.client.put('/api/v1/customers/')
        post = self.client.post('/api/v1/customers/')
        delete = self.client.delete('/api/v1/customers/')
        self.assertEqual(200, get.status_code)
        self.assertEqual(405, put.status_code)
        self.assertNotEqual(405, post.status_code)
        self.assertEqual(405, delete.status_code)

        get = self.client.get('/api/v1/customers/1')
        put = self.client.put('/api/v1/customers/1')
        post = self.client.post('/api/v1/customers/1')
        delete = self.client.delete('/api/v1/customers/1')
        self.assertNotEqual(405, get.status_code)
        self.assertNotEqual(405, put.status_code)
        self.assertEqual(405, post.status_code)
        self.assertNotEqual(405, delete.status_code)


    def test_get_api_customers(self):
        """Test get method on /customer/ endpoint"""
        customers = self.client.get('/api/v1/customers/')
        customers_data = json.loads(customers.data.decode())
        self.assertEqual(0, customers_data['count'])

    def test_post_api_customers(self):
        """
            Test main actions for api

            * POST a customer user for /api/v1/customers/, expect HTTP Status Code 201;
            * GET /api/v1/customers/ expect an JSON with previous POST customer;
            * PUT /api/v1/customers/1 (id of previous customer POST);
            * DELETE /api/v1/customers/1 (id of previous customer POST);
        """
        customer_form = {
            'name': 'Arthur Dent',
            'address': 'West Country',
            'neighborhood': 'South Western',
            'city': 'England',
            'cpf': '00000000000',
            'email': 'dent.arthur@hitchhikers.gal',
            'phone': '00000000',
            'sites': ['https://dontpanic.com', 'http://hitchhikers.gal'],
        }

       #Test sending an invalid data
        bad_request = self.client.post('/api/v1/customers/', data=customer_form)
        self.assertEqual(400, bad_request.status_code)

        post_customer = self.client.post(
            '/api/v1/customers/',
            data=json.dumps(customer_form),
            headers={'Content-type': 'application/json; charset=utf-8'}
        )
        self.assertEqual(201, post_customer.status_code)

        customers = self.client.get('/api/v1/customers/')
        data = json.loads(customers.data.decode())
        site_from_first = data['customers'][0]['sites'][0]

        self.assertEqual(1, data['count'])
        self.assertEqual('Arthur Dent', data['customers'][0]['name'])
        self.assertEqual('https://dontpanic.com', site_from_first['url'])

        customer_put_form = {
            'name': 'Marvin'
        }
        put_customer = self.client.put(
            '/api/v1/customers/1',
            data=json.dumps(customer_put_form),
            headers={'Content-type': 'application/json; charset=utf-8'}
        )
        self.assertEqual(200, put_customer.status_code)

        customers = self.client.get('/api/v1/customers/1')
        data = json.loads(customers.data.decode())

        self.assertEqual(200, customers.status_code)
        self.assertEqual('Marvin', data['customers'][0]['name'])

        delete = self.client.delete('/api/v1/customers/1')
        self.assertEqual(200, delete.status_code)

        customers = self.client.get('/api/v1/customers/1')
        self.assertEqual(404, customers.status_code)



if __name__ == '__main__':
    unittest.main()

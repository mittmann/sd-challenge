import json
from flask import request, abort
from server.app import db
from server.customer.models import Customer, Site
from server.utils.serializer import sqlalchemy_serialize as serializer

import sys

class CustomerView:
    """
        CustomerView
        Class for helper the Customer actions from API request.
    """

    @classmethod
    def _is_valid_field(cls, field):
        """
            Filter for remove SQLAlchemy attributes or protected fields that
            shouldn't inserted to customer on commit (like id or foreign field)
        """
        protected = ['id', 'sites']

        if field in protected or field.startswith('_'):
            return False
        return True

    @classmethod
    def clean(cls, data):
        """
            Return a cleaned dict, with fields that can be inserted on customer
            commit
        """
        customer_fields = Customer.__dict__.keys()
        key_list = [key for key in customer_fields if cls._is_valid_field(key)]

        fields = {}

        for key in key_list:
            try:
                value = data.get(key, None)
                if value:
                    fields[key] = value
            except:
                print(key)
        return fields

    @classmethod
    def get_customer(cls, _id=None):
        """Helper for get a customer or all customers from database"""
        if not _id:
            customer = Customer.query.filter()
        else:
            customer = Customer.query.filter(Customer.id == _id)
            if not customer.count():
                # Return 404 case the customer id not exists on database
                abort(404)

        customer_list = serializer(customer)
        return {'count': len(customer_list), 'customers': customer_list}

    @classmethod
    def insert_customer(cls):
        """Helper for insert a new customer on database"""
        try:
            # Clean fields
            fields = cls.clean(request.json)

            # Get list of sites
            sites = request.json.get('sites')

            if not sites or not fields:
                abort(400)

            # Create and save the new customer
            customer = Customer(**fields)
            db.session.add(customer)
            db.session.commit()

            # Create site instance and associate to customer
            for site in sites:
                db.session.add(Site(site, customer.id))
            db.session.commit()

        except:
            abort(400)

    @classmethod
    def update_customer(cls, _id):
        """Helper for update a customer on database"""
        customer = Customer.query.filter(Customer.id == _id).first()

        if not customer:
            abort(404)

        try:
            fields = cls.clean(request.json)

            # Updates each customer attribute that is setted to update
            for field in fields.keys():
                setattr(customer, field, fields.get(field))
            db.session.merge(customer)
            db.session.commit()
        except:
            abort(400)

    @classmethod
    def delete_customer(cls, _id):
        """Helper for delete a customer from database"""
        customer = Customer.query.filter(Customer.id == _id).first()
        if customer:
            db.session.delete(customer)
            db.session.commit()
        else:
            abort(404)

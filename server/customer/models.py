from server.app import db


class Site(db.Model):
    __tablename__ = 'customer_site'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(250))
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'))

    def __init__(self, url, customer, **kwargs):
        self.url = url
        self.customer_id = customer

    def __repr__(self):
        return '<Site: {0} - {1}>'.format(self.url[:15], self.customer_id)


class Customer(db.Model):
    __tablename__ = 'customer'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    address = db.Column(db.String(100))
    neighborhood = db.Column(db.String(50))
    city = db.Column(db.String(50))
    country = db.Column(db.String(50))
    email = db.Column(db.String(100))
    cpf = db.Column(db.String(11))
    phone = db.Column(db.String(15))
    sites = db.relationship('Site', lazy='subquery')

    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.address = kwargs.get('address')
        self.neighborhood = kwargs.get('neighborhood')
        self.city = kwargs.get('city')
        self.country = kwargs.get('country')
        self.email = kwargs.get('email')
        self.cpf = kwargs.get('cpf')
        self.phone = kwargs.get('phone')

    def __repr__(self):
        return '<Customer: {}>'.format(self.name)

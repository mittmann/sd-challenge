import os

ROOT_PATH = os.path.dirname(os.path.abspath(__name__))
STATIC_DIR = os.path.join(ROOT_PATH, 'www/public')

DEBUG = True

JSON_AS_ASCII = False
SQLITE_FILE = os.path.join(ROOT_PATH, 'app.sqlite3')
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(SQLITE_FILE)

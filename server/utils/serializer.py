from sqlalchemy import inspect


def sqlalchemy_serialize(query):
    """Helper for serialize a SQLAlchemy BaseQuery, returning a dict"""
    query = list(query)
    if not len(query):
        return []

    mapper = inspect(query[0].__mapper__)
    attributes = mapper.columns.keys()
    relationships = mapper.relationships.keys()

    mapped_query = []

    for result in query:
        # serialize default attributes
        result_dict = {key: getattr(result, key) for key in attributes}

        # serialize relationships attributes
        if relationships:
            result_dict.update(
                {key: sqlalchemy_serialize(getattr(result, key)) for key in relationships}
            )
        mapped_query.append(result_dict)

    return mapped_query

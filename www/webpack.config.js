var path = require('path');
var webpack = require('webpack');
 
module.exports = {
  entry: { filename: './src/scripts/main.js' },
  output: { path: './public', filename: 'index.js' },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },
};
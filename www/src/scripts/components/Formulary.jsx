import React from 'react'
import {findDOMNode} from 'react-dom'




class Input extends React.Component {
    constructor(props) {
        super(props);

        this.componentMounted = false
        
        this.state = {
            valid: true,
            empty: false,
            canValidate: false
        }

        this.validate = this.validate.bind(this)
        this.onBlurHaldler = this.onBlurHaldler.bind(this)
        this.onFocusHandler = this.onFocusHandler.bind(this)
        this.onChangeHandler = this.onChangeHandler.bind(this)
    }

    validate(value) {
        if (this.props.required && value.length === 0) {
            this.setState({
                valid: false,
                empty: true,
                validate: true
            })
        } else  {
            this.setState({
                valid: true,
                empty: false,
                validate: true
            })            
        }
    }

    onBlurHaldler(event) {
        this.validate(event.target.value)
    }

    onChangeHandler(event) {}

    onFocusHandler(event) {
        let element = Object.values(this.refs)[0]
        element.className = element.className.replace('shake', '')
        this.setState({
            validate: false
        })
    }

    componentDidMount() {
        this.componentMounted = true
    }

    render() {
        let props = this.props
        let {valid, empty, validate} = this.state
        let className = 'Input';

        if (validate) {
            className += valid ? ' valid' : ' invalid shake'
            className += empty ? ' required shake' : ''
        }

        let inputProps = {
            type: props.type,
            name: props.name,
            defaultValue: props.defaultValue,
            ref: props.name,
            onBlur: this.onBlurHaldler,
            onFocus: this.onFocusHandler,
            onChange: this.onChangeHandler,
        }

        
        return (
            <div className={className}>
                <label className={props.required ? 'required' : ''}>{props.label}:</label>
                <input ref="input" {...inputProps} />
                {props.children}
            </div>
        )
    }
}
Input.defaultProps = {
    label: 'label',
    type: 'text',
    name: 'name',
    defaultValue: '',
    required: true
}


class EmailInput extends Input {
    validate(value) {
        super.validate(value)

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(value)) {
            this.setState({
                valid: false,
            })
        }
    }
}
EmailInput.defaultProps = {
    type: 'email',
}

class URLInput extends Input {
    validate(value) {
        super.validate(value)

        var re = /^(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
        if (!re.test(value)) {
            this.setState({
                valid: false,
            })
        }
    }    
}
URLInput.defaultProps = {
    defaultValue: 'http://',
}


class CPFInput extends Input {
    validate(value) {
        super.validate(value)

        var re = /([0-9]{2}?[0-9]{3}?[0-9]{3}?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}?[0-9]{3}?[0-9]{3}[-]?[0-9]{2})/;
        if (!re.test(value)) {
            this.setState({
                valid: false,
            })
        }
    }

    onChangeHandler(event) {
        let element = event.target
        element.value = element.value.replace(/([^0-9]*)$/, "")
    }
}
URLInput.defaultProps = {
    defaultValue: 'http://',
}







class Formulary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sites: ['url-0'],
            counter: 0,
            postSussess: false
        }
        this.addUrlInput = this.addUrlInput.bind(this)
        this.removeUrlInput = this.removeUrlInput.bind(this)
        this.postForm = this.postForm.bind(this)
    }

    addUrlInput() {
        let {sites, counter} = this.state
        counter++
        sites.push(`url-${counter}`)
        this.setState({
            sites: sites,
            counter: counter
        })
    }

    removeUrlInput() {
        let {sites, counter} = this.state
        if (counter == 0) {
            return false
        }
        sites.splice(sites.indexOf(`url-${counter}`, 1))
        counter--
        this.setState({
            sites: sites,
            counter: counter
        })
    }

    postForm() {
        let {setState} = this

        let form = {
            sites: []
        }
        let sites = []

        Object.keys(this.refs).map(ref => {
            let value = Object.values(this.refs[ref].refs)[0].value
            if (/url/.test(ref)) {
                form.sites.push(value)
            } else {
                form[ref] = value
            }
        })

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status === 201) {
                    this.setState({
                        postSussess: true,
                    })
                }
            }
        }.bind(this)

        xhr.open('POST', '/api/v1/customers/')
        xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        xhr.send(JSON.stringify(form))
    }

    render() {
        let {sites, counter, postSussess} = this.state

        if (postSussess) {
            return (
		<div>
			<p className="success">Seu cadastro foi efetuado com sucesso!</p>
			<a className="success" href="/">Voltar</a>
		</div>
	   )
        } else {
            return (
                <div>
                    <h1>Quer Blindar seu site contra invasões?</h1>
                    <p>Diariamente testamos mais de 35 mil vulnerabilidades em sites, previnindo ataques, infecção por malware, roubo de informações e números de cartão de crédito, oferecendo uma solução completa de proteção para seu site.</p>
                    
                    <div className="FormularyComponent">
                        <Input ref="name" name="name" label="Nome Completo"/>
                        <Input ref="address" name="address" label="Endereço"/>
                        <Input ref="neighborhood" name="neighborhood" label="Bairro"/>
                        <Input ref="city" name="city" label="Cidade"/>
                        <CPFInput ref="cpf" name="cpf" label="CPF (Somente números)"/>
                        <EmailInput ref="email" name="email" label="Email"/>
                        <Input ref="phone" name="phone" label="Telefone (Somente números)"/>

                        {sites.map(site =>
                            <URLInput ref={site} key={site} name={site} label="Sites">
                                <span className="add" onClick={this.addUrlInput}>+</span>
                                {counter > 0
                                    ? <span className="rem" key={counter} onClick={this.removeUrlInput}>-</span>
                                    : ''
                                }
                            </URLInput>
                        )}

                        <button onClick={this.postForm}>Quero blindar meu site</button>
                    </div>
                </div>
            )
        }
    }
}

export default Formulary

import React from 'react'
import ReactDOM from 'react-dom'
import Formulary from './components/Formulary.jsx'


const App = (props) => <Formulary />

ReactDOM.render(<App />, document.querySelector('#app'))
# Desafio Site Blindado

Aplicativo web, desenvolvido em Flask e React.js, para o desafio proposto pelo Site Blindado.

Esse app foi desenvolvido utilizando as seguintes tecnologias:

- **Python 3.4**
- **Flask 0.11.1**

###Como usar:

Levando-se em consideração que o ambiente já esteja preparado, use o seguinte comando para configurar o app:
*Caso esteja em ambiente Unix-Like e tenha o make*:

	$ make install


E para rodar:

	$ make run


*Senão, utilize o comando abaixo*:
	pip install -r requirements/requirements.txt
	python server/app.py


Ambas as formas iniciará o ***werkzeug server***, rodando o APP na URI [http://localhost:5000](http://localhost:5000)

Obs.: O projeto foi desenvolvido e testado em sistemas operacionais Unix-like (Ubuntu e MacOS), não foi testado em ambientes Microsoft.
###Testes
Para executar os testes, basta executar o seguinte comando:
	
	$ make test


### Endpoints da API
A API possui 2 endpoints, seguindo as especificações RERTful:

**/api/v1/customers/**
Retorna a representação dos clientes que se cadastraram no app.


* ```HTTP GET``` Retorna um JSON contendo um atributo ```count``` e um atributo ```customers```, que representa um Array de clientes.

* ```HTTP POST``` Recebe uma request POST, de *content-type application/json*, com um JSON contendo os dados a serem inseridos no banco.


**/api/v1/customers/ID**
Representação de um objeto específico de cliente cadastrado, sendo <ID> o id do cliente no banco de dados.

* ```HTTP GET``` Retorna um JSON contendo um atributo ```count``` e um atributo ```customers```, que representa um Array com as informações do cliente.

* ```HTTP POST``` Recebe uma request POST, de *content-type application/json*, com um JSON contendo os dados a serem atualizados no banco.

* ```HTTP DELETE``` Deleta o cliente no banco de dados.
